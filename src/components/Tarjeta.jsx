import { Card, CardImg } from "reactstrap";

export function Tarjeta({ datos }) {
  return (
    <Card className="col-4 py-3" style={{ display: "flex" }}>
      {datos?.map((integrante) => (
        <div className="d-flex">
          <div className="mb-2">
            <CardImg
              style={{ objectFit: "cover", width: "100px", height: "100px" }}
              className="rounded-circle border-0"
              alt="Card image cap"
              src={integrante.Foto}
            />
          </div>
          <div className="px-5">
            <div>
              <h5>{integrante?.Nombres}</h5>
              <h6>{integrante?.Cargo}</h6>
            </div>
          </div>
        </div>
      ))}
    </Card>
  );
}
