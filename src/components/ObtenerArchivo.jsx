import * as XLSX from "xlsx";

import { hacerGrupos } from "../utils/funciones";

export function ObtenerArchivo({ setData, cantidad_participantes }) {
  const onchange = (event) => {
    const [file] = event.target.files;
    const reader = new FileReader();

    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: "binary" });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const datos = XLSX.utils.sheet_to_json(ws, {});
      setData(hacerGrupos(datos, cantidad_participantes));
      //setData()
    };
    reader.readAsBinaryString(file);
    // console.log('dll');
  };

  return (
    <div>
      <input type="file" accept=".xlsx, .xls" onChange={onchange} />
    </div>
  );
}
