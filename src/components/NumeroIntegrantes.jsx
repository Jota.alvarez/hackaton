export function NumeroIntegrantes({ valor, setValor }) {
  return (
    <div className="mb-3 col-5">
      <input
        type="number"
        className="form-control"
        placeholder="Numero de integrantes"
        value={valor}
        min="1"
        max="10"
        onChange={(evento) => setValor(evento.target.value)}
      />
    </div>
  );
}
