import "./App.css";

import { NumeroIntegrantes } from "./components/NumeroIntegrantes";
import { ObtenerArchivo } from "./components/ObtenerArchivo";
import { Tarjeta } from "./components/Tarjeta";
import { useState } from "react";

function App() {
  const [valor, setValor] = useState("");
  let [data, setData] = useState([]);
  console.log(data);
  return (
    <div className="App container py-4">
      <div className="row">
        <div className="col-6">
          <ObtenerArchivo setData={setData} cantidad_participantes={valor} />
        </div>
        <div className="col-6">
          <NumeroIntegrantes valor={valor} setValor={setValor} />
        </div>
      </div>

      <div className="row">
        {data.map((grupo) => (
          <Tarjeta datos={grupo} />
        ))}
      </div>
    </div>
  );
}

export default App;
